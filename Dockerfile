# build stage
FROM golang:alpine AS build-env
RUN apk --no-cache add build-base git bzr mercurial gcc
ADD . /src
RUN cd /src && go build -o goapp cmd/main.go

# final stage
FROM alpine
WORKDIR /app
COPY --from=build-env /src/goapp /app/
COPY --from=build-env /src/imt2681-project-firebase-adminsdk-oe91e-92ff1253cb.json /
ENTRYPOINT ./goapp
