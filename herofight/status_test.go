package herofight

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

const testURLForStatus = "http://testurl.com"

func TestStatusHandler(t *testing.T) {
	superHeroesAPIInstance = &mockSuperHeroesAPIClient{}
	DatabaseInstance = &mockDatabase{}

	testStatusHandlerWithInvalidRequest(t)
	testStatusHandlerWithValidRequest(t)
}

func testStatusHandlerWithInvalidRequest(t *testing.T) {
	request := httptest.NewRequest(http.MethodPost, testURLForStatus, strings.NewReader("invalid body"))
	responseRecorder := httptest.NewRecorder()
	StatusHandler(responseRecorder, request)
	if responseRecorder.Code != http.StatusMethodNotAllowed {
		t.Error("Invalid request (with POST HTTP method) did not produce Method Not Allowed status code")
	}
}

func testStatusHandlerWithValidRequest(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, testURLForStatus, nil)
	responseRecorder := httptest.NewRecorder()
	StatusHandler(responseRecorder, request)

	if responseRecorder.Code != http.StatusOK {
		t.Error("Valid GET request did not produce OK HTTP status code")
		return
	}

	var returnedStatus Status
	err = json.NewDecoder(responseRecorder.Body).Decode(&returnedStatus)
	if err != nil {
		t.Error(err)
		return
	}

	if http.StatusText(returnedStatus.DatabaseStatusCode) == "" {
		t.Error("Database status code is invalid")
	}

	if http.StatusText(returnedStatus.HeroAPIStatusCode) == "" {
		t.Error("SuperHero API status code is invalid")
	}

	if returnedStatus.Uptime < 0 {
		t.Error("Uptime is invalid")
	}
}
