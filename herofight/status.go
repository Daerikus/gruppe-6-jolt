package herofight

import (
	"encoding/json"
	"net/http"
	"time"
)

const timeOutInSeconds = 3
const heroAPIcheck = heroAPIRoot + "/1"

var statusClient = &http.Client{
	Timeout: time.Second * timeOutInSeconds,
}

var lastStartTime = time.Now().Unix()

// Checks the status of HeroAPI and Firebase
func checkStatus(w http.ResponseWriter) {
	var status Status

	status.HeroAPIStatusCode = getHeroAPIStatus()
	status.DatabaseStatusCode = getDBStatus()
	status.Version = "v1"
	status.Uptime = time.Now().Unix() - lastStartTime

	w.Header().Add("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(status)
	if err != nil {
		http.Error(w, "Error while processing request: "+err.Error(), http.StatusInternalServerError)
	}
}

// gets the statuscode of HeroAPI
func getHeroAPIStatus() int {
	resp, err := statusClient.Get(heroAPIcheck)
	if err != nil {
		return http.StatusServiceUnavailable
	}
	defer resp.Body.Close()

	statusCode := resp.StatusCode

	return statusCode
}

//Gets the statuscode of firebase
func getDBStatus() int {
	return int(DatabaseInstance.GetStatusCode(timeOutInSeconds))
}
