package herofight

import (
	"context"

	"cloud.google.com/go/firestore"
)

// Struct with response string (success or error)
// + array of hero results from the API

// APIResponse (to remove golint complaints)
type APIResponse struct {
	Response string `json:"response"`
	Results  []Hero `json:"results"`
}

// RetrievedHeroes struct with array of returned heroes
type RetrievedHeroes struct {
	Heroes []Hero `json:"heroes"`
}

// Hero (character) information
type Hero struct {
	ID         string     `json:"id"`
	Name       string     `json:"name"`
	Biography  Biography  `json:"biography"`
	Work       Work       `json:"work"`
	Stats      Powerstats `json:"powerstats"`
	Appearance Appearance `json:"appearance"`
}

// Biography for the character
type Biography struct {
	FullName  string `json:"full-name"`
	POB       string `json:"place-of-birth"`
	Alignment string `json:"alignment"`
}

// Work struct with regular job and base
type Work struct {
	Occupation string `json:"occupation"`
	Base       string `json:"base"`
}

// Powerstats for the character
type Powerstats struct {
	Intelligence string `json:"intelligence"`
	Strength     string `json:"strength"`
	Speed        string `json:"speed"`
	Durability   string `json:"durability"`
	Power        string `json:"power"`
	Combat       string `json:"combat"`
}

// Appearance of the character
type Appearance struct {
	Gender    string   `json:"gender"`
	Race      string   `json:"race"`
	Height    []string `json:"height"`
	Weight    []string `json:"weight"`
	Eyecolor  string   `json:"eye-color"`
	Haircolor string   `json:"hair-color"`
}

// RequestTeam struct used to decode team payload
type RequestTeam struct {
	Name   string   `json:"name"`
	Heroes []string `json:"characters"`
}

// Team struct with array of hero names and power number
type Team struct {
	Name       string   `json:"name"`
	TotalPower int      `json:"totalpower"`
	Heroes     []string `json:"characters"`
}

// Fight results
type Fight struct {
	Team1           string `json:"team1"`
	Team1FightPower int    `json:"team1points"`
	Team1Luck       string `json:"team1luck"`
	Team2           string `json:"team2"`
	Team2FightPower int    `json:"team2points"`
	Team2Luck       string `json:"team2luck"`
	Winner          string `json:"winner"`
}

// FirestoreDB struct
type FirestoreDB struct {
	Ctx    context.Context
	Client *firestore.Client
}

// Status struct
type Status struct {
	HeroAPIStatusCode  int    `json:"heroapi"`
	DatabaseStatusCode int    `json:"database"`
	Uptime             int64  `json:"uptime"`
	Version            string `json:"version"`
}
