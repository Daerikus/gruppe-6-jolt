package herofight

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"
)

const testURLForFights = "http://testurl.com"

func TestHeroFight(t *testing.T) {
	teamNames := []string{"team 1", "team 2"}
	heroes := []Hero{
		firstHero,
		secondHero,
		thirdHero,
	}
	teams := []Team{
		{
			Name:       teamNames[0],
			TotalPower: 400,
			Heroes:     []string{firstHero.Name, secondHero.Name},
		},
		{
			Name:       teamNames[1],
			TotalPower: 324,
			Heroes:     []string{firstHero.Name, thirdHero.Name},
		},
	}
	DatabaseInstance = &mockDatabase{
		heroes: heroes,
		teams:  teams,
	}

	createdFights := heroFight(teamNames)

	if len(createdFights) != 1 {
		t.Error("Wrong number of fights returned from heroFight")
		return
	}

	createdFight := createdFights[0]
	if !((createdFight.Team1 == teamNames[0] && createdFight.Team2 == teamNames[1]) ||
		(createdFight.Team1 == teamNames[1] && createdFight.Team2 == teamNames[0])) {
		t.Error("The team names in the created fight do not match, got: ", createdFight.Team1, createdFight.Team2)
		return
	}
	if createdFight.Team1 == teamNames[0] {
		if createdFight.Team1FightPower < createdFight.Team2FightPower {
			t.Error("Wrong team fight power calculation")
		}
	} else {
		if createdFight.Team2FightPower < createdFight.Team1FightPower {
			t.Error("Wrong team fight power calculation")
		}
	}
	if createdFight.Winner != teamNames[0] {
		t.Error("Wrong winner returned from heroFight")
	}
}

func TestFightHandler(t *testing.T) {
	teamNames := []string{"team 1", "team 2"}
	heroes := []Hero{
		firstHero,
		secondHero,
		thirdHero,
	}
	teams := []Team{
		{
			Name:       teamNames[0],
			TotalPower: 396,
			Heroes:     []string{firstHero.Name, secondHero.Name},
		},
		{
			Name:       teamNames[1],
			TotalPower: 324,
			Heroes:     []string{firstHero.Name, thirdHero.Name},
		},
	}
	fights := []Fight{
		{
			Team1:           teamNames[0],
			Team1FightPower: 400,
			Team1Luck:       "Original power: 396, gained 4 due to good luck",
			Team2:           teamNames[1],
			Team2FightPower: 324,
			Team2Luck:       "Fought at original power, no luck involved",
			Winner:          teamNames[0],
		},
	}
	DatabaseInstance = &mockDatabase{
		heroes: heroes,
		teams:  teams,
		fights: fights,
	}

	testFightHandlerWithInvalidRequest(t)
	testFightHandlerWithValidGetRequest(t, fights)
	testFightHandlerWithValidPostRequest(t, teamNames)
}

func testFightHandlerWithInvalidRequest(t *testing.T) {
	request := httptest.NewRequest(http.MethodPut, testURLForFights, strings.NewReader("invalid body"))
	responseRecorder := httptest.NewRecorder()
	FightHandler(responseRecorder, request)
	if responseRecorder.Code != http.StatusMethodNotAllowed {
		t.Error("Invalid request (with PUT HTTP method) did not produce Method Not Allowed status code")
	}
}

func testFightHandlerWithValidGetRequest(t *testing.T, fights []Fight) {
	request := httptest.NewRequest(http.MethodGet, testURLForFights, nil)
	responseRecorder := httptest.NewRecorder()
	FightHandler(responseRecorder, request)

	if responseRecorder.Code != http.StatusOK {
		t.Error("Valid GET request did not produce OK HTTP status code")
		return
	}

	var retrievedFights []Fight
	err = json.NewDecoder(responseRecorder.Body).Decode(&retrievedFights)
	if err != nil {
		t.Error(err)
		return
	}

	if len(retrievedFights) != 1 {
		t.Error("Wrong number of fights returned")
		return
	}

	if !reflect.DeepEqual(retrievedFights, fights) {
		t.Error("Retrieved fights do not match")
	}
}

func testFightHandlerWithValidPostRequest(t *testing.T, teamNames []string) {
	body := "[\"" + teamNames[0] + "\", \"" + teamNames[1] + "\"]"
	request := httptest.NewRequest(http.MethodPost, testURLForFights, strings.NewReader(body))
	responseRecorder := httptest.NewRecorder()
	FightHandler(responseRecorder, request)

	if responseRecorder.Code != http.StatusCreated {
		t.Error("Valid POST request did not produce Status Created HTTP status code")
		return
	}

	var returnedFights []Fight
	err = json.NewDecoder(responseRecorder.Body).Decode(&returnedFights)
	if err != nil {
		t.Error(err)
		return
	}

	if len(returnedFights) != 1 {
		t.Error("Wrong number of fights returned from FightHandler")
		return
	}
}
