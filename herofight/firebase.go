package herofight

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	firebase "firebase.google.com/go"
	"github.com/pkg/errors"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

// The file path of the JSON file that contains the credentials
// that are used to connect to FireStore.
// Needs to be var instead of const to enable the tests
// to override it with a dummy credentials file path.
var credentialsFilePath = "../imt2681-project-firebase-adminsdk-oe91e-92ff1253cb.json"

// DatabaseInstance (to remove golint complaints)
var DatabaseInstance Database = &FirestoreDB{} // Firestore struct

var dummyDocumentID = "ypwBC4zndZy1OkZDUpO0"

// Database interface that provides all the methods needed to
// access persisted entities (such as heroes and Teams).
type Database interface {
	InitDB()
	GetAllDBHeroes() ([]Hero, error)
	GetAllDBTeams() ([]Team, error)
	GetAllDBFights() ([]Fight, error)
	SaveHero(hero *Hero) error
	SaveTeam(team *Team) error
	SaveFight(fight *Fight) error
	GetStatusCode(timeOut uint) uint16
}

// mockDatabase is a default mock of the database
// which contains empty implementations of the Database interface
type mockDatabase struct {
	heroes []Hero
	teams  []Team
	fights []Fight
}

// All of the following methods do nothing, except for returning empty data where needed.
// InitDB (to remove golint complaints)
func (d *mockDatabase) InitDB() {}

// GetAllDBHeroes (to remove golint complaints)
func (d *mockDatabase) GetAllDBHeroes() ([]Hero, error) {
	return d.heroes, nil
}

// GetAllDBTeams
func (d *mockDatabase) GetAllDBTeams() ([]Team, error) {
	return d.teams, nil
}

// GetAllDBFights
func (d *mockDatabase) GetAllDBFights() ([]Fight, error) {
	return d.fights, nil
}

// SaveHero
func (d *mockDatabase) SaveHero(hero *Hero) error {
	d.heroes = append(d.heroes, *hero)
	return nil
}

// SaveTeam
func (d *mockDatabase) SaveTeam(team *Team) error {
	d.teams = append(d.teams, *team)
	return nil
}

// SaveFight
func (d *mockDatabase) SaveFight(fight *Fight) error {
	d.fights = append(d.fights, *fight)
	return nil
}

// GetStatusCode
func (d *mockDatabase) GetStatusCode(timeOut uint) uint16 {
	return http.StatusOK
}

// InitDB initializes database
func (d *FirestoreDB) InitDB() {
	d.Ctx = context.Background()

	// Uses service account accessed by credentials file
	serviceAccount := option.WithCredentialsFile(credentialsFilePath)
	app, err := firebase.NewApp(d.Ctx, nil, serviceAccount)

	if err != nil {
		log.Fatalln("Error initializing app: ", err)
	}

	d.Client, err = app.Firestore(d.Ctx)

	if err != nil {
		log.Fatalln(err)
	}
}

// GetAllDBHeroes retrieves all characters from database
func (d *FirestoreDB) GetAllDBHeroes() ([]Hero, error) {
	var returnedHeroes []Hero
	var heroStruct Hero

	iter := d.Client.Collection("characters").Documents(d.Ctx)

	for {
		heroStruct = Hero{}

		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}

		err = doc.DataTo(&heroStruct) // Saves data in heroStruct
		if err != nil {
			return nil, err
		}

		returnedHeroes = append(returnedHeroes, heroStruct)
	}

	return returnedHeroes, nil
}

// GetAllDBTeams retrieves all teams from database
func (d *FirestoreDB) GetAllDBTeams() ([]Team, error) {
	var returnedTeams []Team
	var teamStruct Team

	iter := d.Client.Collection("teams").Documents(d.Ctx)

	for {
		teamStruct = Team{}
		doc, err := iter.Next()

		if err == iterator.Done {
			break
		}

		if err != nil {
			return nil, err
		}

		err = doc.DataTo(&teamStruct) // Saves data in teamStruct

		if err != nil {
			return nil, err
		}

		returnedTeams = append(returnedTeams, teamStruct)
	}

	return returnedTeams, nil
}

// GetAllDBFights retrieves all fights from database
func (d *FirestoreDB) GetAllDBFights() ([]Fight, error) {
	var returnedFights []Fight
	var fightStruct Fight

	iter := d.Client.Collection("fights").Documents(d.Ctx)

	for {
		fightStruct = Fight{}
		doc, err := iter.Next()

		if err == iterator.Done {
			break
		}

		if err != nil {
			return nil, err
		}

		err = doc.DataTo(&fightStruct) // Saves data in teamStruct

		if err != nil {
			return nil, err
		}

		returnedFights = append(returnedFights, fightStruct)
	}

	return returnedFights, nil
}

// SaveHero creates document that saves hero
func (d *FirestoreDB) SaveHero(hero *Hero) error {
	newDoc := d.Client.Collection("characters").NewDoc() // Creates new document
	_, err := newDoc.Set(d.Ctx, hero)                    // Sets context to hero

	if err != nil {
		fmt.Println("Error saving hero to Firestore database: ", err)
		return errors.Wrap(err, "Error in SaveHero()")
	}
	return nil
}

// SaveTeam creates document that saves team
func (d *FirestoreDB) SaveTeam(team *Team) error {
	newDoc := d.Client.Collection("teams").NewDoc() // Creates new document
	_, err := newDoc.Set(d.Ctx, team)               // Sets context to team

	if err != nil {
		fmt.Println("Error saving team to Firestore database: ", err)
		return errors.Wrap(err, "Error in SaveTeam()")
	}
	return nil
}

// SaveFight creates document that saves team
func (d *FirestoreDB) SaveFight(fight *Fight) error {
	newDoc := d.Client.Collection("fights").NewDoc() // Creates new document
	_, err := newDoc.Set(d.Ctx, fight)               // Sets context to fight

	if err != nil {
		fmt.Println("Error saving fight to Firestore database: ", err)
		return errors.Wrap(err, "Error in SaveFight()")
	}
	return nil
}

// GetStatusCode gets statuscode of firebase
func (d *FirestoreDB) GetStatusCode(timeOut uint) uint16 {
	resultChannel := make(chan uint16)
	timeout := time.After(time.Duration(timeOut) * time.Second)
	go d.checkDummyDocument(resultChannel)
	select {
	case result := <-resultChannel:
		return result
	case <-timeout:
		return http.StatusServiceUnavailable
	}
}

// Checks a dummy document in firebase
func (d *FirestoreDB) checkDummyDocument(resultChannel chan<- uint16) {
	_, err := d.Client.Collection("dummy").Doc(dummyDocumentID).Get(d.Ctx)
	if err != nil {
		log.Println("Failed to get dummy document when checking status: " + err.Error())
		resultChannel <- http.StatusServiceUnavailable
	}
	resultChannel <- http.StatusOK
	close(resultChannel)
}
