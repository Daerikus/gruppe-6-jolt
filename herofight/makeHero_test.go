package herofight

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"
)

func TestHeroesHandler(t *testing.T) {
	DatabaseInstance = &mockDatabase{
		heroes: []Hero{},
		teams:  []Team{},
	}

	testHeroesHandlerWithInvalidRequest(t)
	testHeroesHandlerWithValidPostRequest(t)
}

func testHeroesHandlerWithInvalidRequest(t *testing.T) {
	request := httptest.NewRequest(http.MethodPut, testURLForHeroes, strings.NewReader("invalid body"))
	responseRecorder := httptest.NewRecorder()
	HeroesHandler(responseRecorder, request)
	if responseRecorder.Code != http.StatusMethodNotAllowed {
		t.Error("Invalid request (with PUT HTTP method) did not produce Method Not Allowed status code")
	}
}

func testHeroesHandlerWithValidPostRequest(t *testing.T) {
	body, err := json.Marshal(firstHero)
	if err != nil {
		t.Error(err)
		return
	}
	request := httptest.NewRequest(http.MethodPost, testURLForHeroes, strings.NewReader(string(body)))
	responseRecorder := httptest.NewRecorder()

	HeroesHandler(responseRecorder, request)
	if responseRecorder.Code != http.StatusCreated {
		t.Error("Valid POST request did not produce Status Created HTTP status code")
	}
}

func TestMakeHero(t *testing.T) {
	DatabaseInstance = &mockDatabase{
		heroes: []Hero{},
		teams:  []Team{},
	}

	testMakeHeroWithInvalidRequest(t)
	testMakeHeroWithValidRequest(t)
}

func testMakeHeroWithInvalidRequest(t *testing.T) {
	testMakeHeroWithEmptyBody(t)
	testMakeHeroWithInvalidBody(t)
	testMakeHeroWithEmptyName(t)
	testMakeHeroWithEmptyPower(t)
}

func testMakeHeroWithEmptyBody(t *testing.T) {
	request := httptest.NewRequest(http.MethodPost, testURLForHeroes, nil)
	responseRecorder := httptest.NewRecorder()
	makeHero(responseRecorder, request)

	if responseRecorder.Code != http.StatusBadRequest {
		t.Error("Request with an invalid body did not produce Bad Request HTTP status code")
	}
}

func testMakeHeroWithInvalidBody(t *testing.T) {
	request := httptest.NewRequest(http.MethodPost, testURLForHeroes, strings.NewReader("invalid body"))
	responseRecorder := httptest.NewRecorder()
	makeHero(responseRecorder, request)

	if responseRecorder.Code != http.StatusBadRequest {
		t.Error("Request with an invalid body did not produce Bad Request HTTP status code")
	}
}

func testMakeHeroWithEmptyName(t *testing.T) {
	invalidHero := secondHero
	invalidHero.Name = ""
	body, err := json.Marshal(invalidHero)
	if err != nil {
		t.Error(err)
		return
	}
	request := httptest.NewRequest(http.MethodPost, testURLForHeroes, strings.NewReader(string(body)))
	responseRecorder := httptest.NewRecorder()
	makeHero(responseRecorder, request)

	if responseRecorder.Code != http.StatusBadRequest {
		t.Error("Request with an invalid body did not produce Bad Request HTTP status code")
	}
}

func testMakeHeroWithEmptyPower(t *testing.T) {
	invalidHero := secondHero
	invalidHero.Stats.Power = ""
	body, err := json.Marshal(invalidHero)
	if err != nil {
		t.Error(err)
		return
	}
	request := httptest.NewRequest(http.MethodPost, testURLForHeroes, strings.NewReader(string(body)))
	responseRecorder := httptest.NewRecorder()
	makeHero(responseRecorder, request)

	if responseRecorder.Code != http.StatusBadRequest {
		t.Error("Request with an invalid body did not produce Bad Request HTTP status code")
	}
}

func testMakeHeroWithValidRequest(t *testing.T) {
	body, err := json.Marshal(secondHero)
	if err != nil {
		t.Error(err)
		return
	}
	request := httptest.NewRequest(http.MethodPost, testURLForHeroes, strings.NewReader(string(body)))
	responseRecorder := httptest.NewRecorder()
	makeHero(responseRecorder, request)

	if responseRecorder.Code != http.StatusCreated {
		t.Error("Valid POST request did not produce Status Created HTTP status code")
	}

	savedHeroes, err := DatabaseInstance.GetAllDBHeroes()
	if err != nil {
		t.Error(err)
		return
	}
	if len(savedHeroes) != 1 {
		t.Error("Wrong number of heroes in database after creating one")
		return
	}
	if !reflect.DeepEqual(savedHeroes[0], secondHero) {
		t.Error("Saved hero does not match")
	}
}
