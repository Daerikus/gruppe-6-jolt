package herofight

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"
)

const testURLForTeams = "http://testurl.com"

func TestTeamUpHeroes(t *testing.T) {
	heroes := []Hero{
		firstHero,
		secondHero,
	}
	DatabaseInstance = &mockDatabase{
		heroes: heroes,
	}

	newTeamName := "team 1"
	returnedTeam := teamUpHeroes(newTeamName, []string{"first hero", "second hero"})

	if returnedTeam.Name != newTeamName {
		t.Error("Returned team name does not match when creating a team from hero names")
	}

	returnedHeroNames := returnedTeam.Heroes
	if len(returnedHeroNames) != 2 {
		t.Error("Wrong number of heroes returned when creating a team from hero names")
		return
	}
	if !((returnedHeroNames[0] == firstHero.Name && returnedHeroNames[1] == secondHero.Name) ||
		(returnedHeroNames[1] == firstHero.Name && returnedHeroNames[0] == secondHero.Name)) {
		t.Error("Returned hero names do not match when creating a team from hero names")
	}
}

func TestTeamHandler(t *testing.T) {
	heroes := []Hero{
		firstHero,
		secondHero,
	}
	teams := []Team{
		{
			Name:       "team 1",
			TotalPower: 324,
			Heroes:     []string{firstHero.Name, secondHero.Name},
		},
	}
	DatabaseInstance = &mockDatabase{
		heroes: heroes,
		teams:  teams,
	}

	testTeamHandlerWithInvalidRequest(t)
	testTeamHandlerWithValidGetRequest(t, teams)
	testTeamHandlerWithValidPostRequest(t)
}

func testTeamHandlerWithInvalidRequest(t *testing.T) {
	request := httptest.NewRequest(http.MethodPut, testURLForTeams, strings.NewReader("invalid body"))
	responseRecorder := httptest.NewRecorder()
	TeamHandler(responseRecorder, request)
	if responseRecorder.Code != http.StatusMethodNotAllowed {
		t.Error("Invalid request (with PUT HTTP method) did not produce Method Not Allowed status code")
	}
}

func testTeamHandlerWithValidGetRequest(t *testing.T, teams []Team) {
	request := httptest.NewRequest(http.MethodGet, testURLForTeams, nil)
	responseRecorder := httptest.NewRecorder()
	TeamHandler(responseRecorder, request)

	if responseRecorder.Code != http.StatusOK {
		t.Error("Valid GET request did not produce OK HTTP status code")
		return
	}

	var retrievedTeams []Team
	err = json.NewDecoder(responseRecorder.Body).Decode(&retrievedTeams)
	if err != nil {
		t.Error(err)
		return
	}

	if len(retrievedTeams) != 1 {
		t.Error("Wrong number of teams returned")
		return
	}

	if !reflect.DeepEqual(retrievedTeams, teams) {
		t.Error("Returned teams do not match")
	}
}

func testTeamHandlerWithValidPostRequest(t *testing.T) {
	body := "{\"name\": \"team 2\", \"characters\": [\"first hero\"]}"
	request := httptest.NewRequest(http.MethodPost, testURLForTeams, strings.NewReader(body))
	responseRecorder := httptest.NewRecorder()
	TeamHandler(responseRecorder, request)

	if responseRecorder.Code != http.StatusCreated {
		t.Error("Valid POST request did not produce Status Created HTTP status code")
		return
	}

	var returnedTeam Team
	err = json.NewDecoder(responseRecorder.Body).Decode(&returnedTeam)
	if err != nil {
		t.Error(err)
		return
	}

	if returnedTeam.Name != "team 2" {
		t.Error("Wrong team name returned")
	}

	if len(returnedTeam.Heroes) != 1 {
		t.Error("Wrong number of team members returned")
		return
	}

	if returnedTeam.Heroes[0] != firstHero.Name {
		t.Error("Returned team members do not match")
	}
}
