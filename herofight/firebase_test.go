package herofight

import (
	"os"
	"reflect"
	"testing"
)

const testCredentialsFilePath = "./test_credentials.json"

func TestFirestoreDatabase(t *testing.T) {
	DatabaseInstance = &FirestoreDB{}
	if os.Getenv("CI") != "" {
		credentialsFilePath = testCredentialsFilePath
	}
	DatabaseInstance.InitDB()

	testFirestoreDatabaseHeroes(t)
	testFirestoreDatabaseTeams(t)
	testFirestoreDatabaseFights(t)
}

func testFirestoreDatabaseHeroes(t *testing.T) {
	err := DatabaseInstance.SaveHero(&firstHero)
	if err != nil {
		t.Errorf("ERROR: failed to save hero\n%v\n", err)
		return
	}

	retrievedHeroes, err := DatabaseInstance.GetAllDBHeroes()
	if err != nil {
		t.Errorf("ERROR: failed to retrieve all heroes\n%v\n", err)
		return
	}
	if len(retrievedHeroes) < 1 {
		t.Error("Wrong number of heroes returned")
		return
	}

	savedHeroFound := false
	for _, retrievedHero := range retrievedHeroes {
		if reflect.DeepEqual(retrievedHero, firstHero) {
			savedHeroFound = true
		}
	}
	if savedHeroFound == false {
		t.Error("GetAllDBHeroes did not return the previously saved hero")
	}
}

func testFirestoreDatabaseTeams(t *testing.T) {
	team := Team{
		Name:       "First Team",
		TotalPower: 234324,
		Heroes:     []string{firstHero.Name, secondHero.Name},
	}
	err := DatabaseInstance.SaveTeam(&team)
	if err != nil {
		t.Errorf("ERROR: failed to save team\n%v\n", err)
		return
	}

	retrievedTeams, err := DatabaseInstance.GetAllDBTeams()
	if err != nil {
		t.Errorf("ERROR: failed to retrieve all teams\n%v\n", err)
		return
	}
	if len(retrievedTeams) < 1 {
		t.Error("Wrong number of teams returned: ", len(retrievedTeams))
		return
	}

	savedTeamFound := false
	for _, retrievedTeam := range retrievedTeams {
		if reflect.DeepEqual(retrievedTeam, team) {
			savedTeamFound = true
		}
	}
	if savedTeamFound == false {
		t.Error("GetAllDBTeams did not return the previously saved team")
	}
}

func testFirestoreDatabaseFights(t *testing.T) {
	fight := Fight{
		Team1:           "team 1",
		Team1FightPower: 200,
		Team1Luck:       "Original power: 199, gained 1 due to good luck",
		Team2:           "team 2",
		Team2FightPower: 677,
		Team2Luck:       "Original power: 679, lost 2 due to bad luck",
		Winner:          "team 2",
	}
	err := DatabaseInstance.SaveFight(&fight)
	if err != nil {
		t.Errorf("ERROR: failed to save fight\n%v\n", err)
		return
	}

	retrievedFights, err := DatabaseInstance.GetAllDBFights()
	if err != nil {
		t.Errorf("ERROR: failed to retrieve all fights\n%v\n", err)
		return
	}
	if len(retrievedFights) < 1 {
		t.Error("Wrong number of fights returned: ", len(retrievedFights))
		return
	}

	savedFightFound := false
	for _, retrievedFight := range retrievedFights {
		if reflect.DeepEqual(retrievedFight, fight) {
			savedFightFound = true
		}
	}
	if savedFightFound == false {
		t.Error("GetAllDBFights did not return the previously saved fight")
	}
}
