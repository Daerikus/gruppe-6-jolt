package herofight

import (
	"fmt"
	"net/http"
	"strings"
)

// Retrieves heroes from the Superhero API
func getHeroes(chosenHeroes []string) RetrievedHeroes {
	var retrievedHeroes RetrievedHeroes // Struct with array of returned heroes
	var heroResult Hero                 // Hero struct that is saved in the database

	// Loops through names in the request body array
	for i := 0; i < len(chosenHeroes); i++ {
		// Searches for characters with the payload names
		searchHeroesResult, err := superHeroesAPIInstance.SearchHeroes(chosenHeroes[i])
		if err != nil {
			break
		}

		// Loops through all results in the result array
		for j := 0; j < len(searchHeroesResult); j++ {
			heroResult = searchHeroesResult[j]

			// If the name in the request body is the same as the result name,
			// append the hero to the returned array, and save it in database
			if strings.EqualFold(heroResult.Name, chosenHeroes[i]) {
				retrievedHeroes.Heroes = append(retrievedHeroes.Heroes, heroResult)

				err = DatabaseInstance.SaveHero(&heroResult)

				if err != nil {
					fmt.Println("Error saving hero struct in Firestore: "+err.Error(), http.StatusBadRequest)
				}
			}
		}
	}

	return retrievedHeroes
}
