package herofight

import (
	"strconv"
	"strings"
)

var err error

func teamUpHeroes(name string, chosenHeroes []string) Team {
	var team Team      // Struct with complete team data
	var totalPower int // A team's combined power number

	// Gets all characters from database, function is in firebase.go
	var databaseHeroes, _ = DatabaseInstance.GetAllDBHeroes()

	team.Name = name // Team name = parameter name

	// Loops through names/IDs in parameter array,
	// inner loop goes through all characters in database
	for i := 0; i < len(chosenHeroes); i++ {
		for j := 0; j < len(databaseHeroes); j++ {
			// Checks if string in parameter array is a number
			if isNumber(chosenHeroes[i]) {
				// Compares with database character ID
				if databaseHeroes[j].ID == chosenHeroes[i] {
					// Finds character's total power number and appends their name to team name array
					totalPower += totalHeroPower(databaseHeroes[j])
					team.Heroes = append(team.Heroes, databaseHeroes[j].Name)
				}
				// If not a number
			} else {
				// Compares with database character name
				if strings.EqualFold(databaseHeroes[j].Name, chosenHeroes[i]) {
					// Finds character's total power number and appends their name to team name array
					totalPower += totalHeroPower(databaseHeroes[j])
					team.Heroes = append(team.Heroes, databaseHeroes[j].Name)
				}
			}
		}
	}

	team.TotalPower = totalPower           // Assigns team's total power number
	err = DatabaseInstance.SaveTeam(&team) // Saves team in database

	return team
}

// Checks if string is a number
func isNumber(s string) bool {
	_, err = strconv.ParseFloat(s, 64)
	return err == nil
}

// Calculates total power number for one character
func totalHeroPower(hero Hero) int {
	var powerNumber = 0

	intelligence, _ := strconv.Atoi(hero.Stats.Intelligence)
	strength, _ := strconv.Atoi(hero.Stats.Strength)
	speed, _ := strconv.Atoi(hero.Stats.Speed)
	durability, _ := strconv.Atoi(hero.Stats.Durability)
	power, _ := strconv.Atoi(hero.Stats.Power)
	combat, _ := strconv.Atoi(hero.Stats.Combat)

	powerNumber += intelligence
	powerNumber += strength
	powerNumber += speed
	powerNumber += durability
	powerNumber += power
	powerNumber += combat

	return powerNumber
}
