package herofight

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

const token = "2482089108672750"
const heroAPIRoot = "https://superheroapi.com/api/" + token
const heroAPISearch = heroAPIRoot + "/search/"
const successMessage = "success"

type superHeroesAPIClientInterface interface {
	SearchHeroes(heroName string) ([]Hero, error)
}

var superHeroesAPIInstance superHeroesAPIClientInterface = &superHeroesAPIClient{}

type mockSuperHeroesAPIClient struct {
	heroes []Hero
}

func (c *mockSuperHeroesAPIClient) SearchHeroes(heroName string) ([]Hero, error) {
	result := make([]Hero, 0)
	for _, hero := range c.heroes {
		if strings.Contains(strings.ToLower(hero.Name), heroName) {
			result = append(result, hero)
		}
	}
	return result, nil
}

type superHeroesAPIClient struct{}

func (c *superHeroesAPIClient) SearchHeroes(heroName string) ([]Hero, error) {
	var response APIResponse // Struct with response string, success or error
	encodedHeroName := url.PathEscape(heroName)
	searchURL := heroAPISearch + encodedHeroName
	request, err := http.NewRequest(http.MethodGet, searchURL, nil)

	if err != nil {
		fmt.Println("Error with api url", err)
		return nil, err
	}

	resp, err := http.DefaultClient.Do(request)
	if err != nil {
		fmt.Println("Error with api url", err)
		return nil, err
	}

	defer resp.Body.Close()
	// Decodes response string
	err = json.NewDecoder(resp.Body).Decode(&response)

	if err != nil {
		fmt.Println("Error decoding API response", err)
		return nil, err
	}

	if response.Response == successMessage {
		// Returns results array if the search is successful
		return response.Results, nil
	}
	return nil, errors.New("Character with name " + heroName + " not found")
}
