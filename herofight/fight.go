package herofight

import (
	"math/rand"
	"strconv"
	"strings"
	"time"
)

const minRandomPoints = -5 // Used to generate random number between -5 and 5
const maxRandomPoints = 5

func heroFight(chosenTeams []string) []Fight {
	var fightTeams []Team
	var team1 Team
	var team2 Team
	var fight Fight
	var fights []Fight
	var arrLength int
	var luck int

	var databaseTeams, _ = DatabaseInstance.GetAllDBTeams()

	// Loops through names in parameter array,
	// inner loop goes through all teams in database
	for i := 0; i < len(chosenTeams); i++ {
		for j := 0; j < len(databaseTeams); j++ {
			// If names match, add team to fightTeams array
			if strings.EqualFold(databaseTeams[j].Name, chosenTeams[i]) {
				fightTeams = append(fightTeams, databaseTeams[j])
			}
		}
	}

	// If length of fightTeams array is an odd number, shorten length by one,
	// means that last team gets excluded
	arrLength = len(fightTeams)
	if arrLength%2 != 0 {
		arrLength--
	}

	// Creates fights between two and two teams
	for i := 0; i < arrLength; i += 2 {
		team1 = fightTeams[i]
		team2 = fightTeams[i+1]

		luck = luckPoints()
		fight.Team1 = team1.Name
		fight.Team1FightPower = team1.TotalPower + luck
		fight.Team1Luck = luckMessage(team1.TotalPower, luck)

		luck = luckPoints()
		fight.Team2 = team2.Name
		fight.Team2FightPower = team2.TotalPower + luck
		fight.Team2Luck = luckMessage(team2.TotalPower, luck)

		// The winner is the team with the highest power number
		switch {
		case fight.Team1FightPower > fight.Team2FightPower:
			fight.Winner = fight.Team1
		case fight.Team2FightPower > fight.Team1FightPower:
			fight.Winner = fight.Team2
		default:
			fight.Winner = "Tie. The teams stopped fighting and became friends!"
		}

		fights = append(fights, fight)

		err = DatabaseInstance.SaveFight(&fight)
	}

	return fights
}

func luckPoints() int {
	// Random number from -5 to 5, "luck" in a fight
	rand.Seed(time.Now().UnixNano()) // Seeds random generator
	luck := (rand.Intn(maxRandomPoints-minRandomPoints+1) + minRandomPoints)

	return luck
}

// Creates message that shows original power number and the addition or subtraction of luck number
func luckMessage(origPoints int, luck int) string {
	var message string
	var origPointsStr string
	var luckStr string

	origPointsStr = strconv.Itoa(origPoints)
	luckStr = strconv.Itoa(abs(luck)) // Absolute value of luck number

	// Writes whether power number was subtracted from or added to, or if no luck is involved
	switch {
	case luck > 0:
		message = "Original power: " + origPointsStr + ", gained " + luckStr + " due to good luck"
	case luck < 0:
		message = "Original power: " + origPointsStr + ", lost " + luckStr + " due to bad luck"
	default:
		message = "Fought at original power, no luck involved"
	}

	return message
}

// Finds absolute value of int
func abs(number int) int {
	if number < 0 {
		return -number
	}

	return number
}
