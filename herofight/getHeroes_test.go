package herofight

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"
)

const testURLForHeroes = "http://testurl.com"

var firstHero = Hero{
	ID:   "1",
	Name: "First Hero",
	Biography: Biography{
		FullName:  "First Hero Full",
		POB:       "First place of birth",
		Alignment: "First alignment",
	},
	Work: Work{
		Occupation: "First occupation",
		Base:       "First base",
	},
	Stats: Powerstats{
		Intelligence: "45",
		Strength:     "0",
		Speed:        "55",
		Durability:   "88",
		Power:        "77",
		Combat:       "21",
	},
	Appearance: Appearance{
		Gender:    "First gender",
		Race:      "First race",
		Height:    []string{"1.8m", "5'11"},
		Weight:    []string{"70kg", "154lbs"},
		Eyecolor:  "First eye color",
		Haircolor: "First hair color",
	},
}
var secondHero = Hero{
	ID:   "2",
	Name: "Second Hero",
	Biography: Biography{
		FullName:  "Second Hero Full",
		POB:       "Second place of birth",
		Alignment: "Second alignment",
	},
	Work: Work{
		Occupation: "Second occupation",
		Base:       "Second base",
	},
	Stats: Powerstats{
		Intelligence: "1",
		Strength:     "6",
		Speed:        "88",
		Durability:   "100",
		Power:        "3",
		Combat:       "63",
	},
	Appearance: Appearance{
		Gender:    "Second gender",
		Race:      "Second race",
		Height:    []string{"1.7m", "5'10"},
		Weight:    []string{"80kg", "176lbs"},
		Eyecolor:  "Second eye color",
		Haircolor: "Second hair color",
	},
}
var thirdHero = Hero{
	ID:   "3",
	Name: "Third Hero",
	Biography: Biography{
		FullName:  "Third Hero Full",
		POB:       "Third place of birth",
		Alignment: "Third alignment",
	},
	Work: Work{
		Occupation: "Third occupation",
		Base:       "Third base",
	},
	Stats: Powerstats{
		Intelligence: "1",
		Strength:     "6",
		Speed:        "88",
		Durability:   "100",
		Power:        "3",
		Combat:       "63",
	},
	Appearance: Appearance{
		Gender:    "Third gender",
		Race:      "Third race",
		Height:    []string{"1.7m", "5'10"},
		Weight:    []string{"80kg", "176lbs"},
		Eyecolor:  "Third eye color",
		Haircolor: "Third hair color",
	},
}

func TestGetHeroes(t *testing.T) {
	superHeroesAPIInstance = &mockSuperHeroesAPIClient{
		heroes: []Hero{
			firstHero,
			secondHero,
		},
	}
	DatabaseInstance = &mockDatabase{}

	returnedHeroes := getHeroes([]string{"second hero"}).Heroes

	if len(returnedHeroes) != 1 {
		t.Error("Wrong number of heroes returned")
		return
	}
	if !reflect.DeepEqual(returnedHeroes[0], secondHero) {
		t.Error("Returned hero does not match")
	}
}

func TestProcessGetHeroesRequest(t *testing.T) {
	superHeroesAPIInstance = &mockSuperHeroesAPIClient{
		heroes: []Hero{
			firstHero,
			secondHero,
		},
	}
	DatabaseInstance = &mockDatabase{}

	request := httptest.NewRequest(http.MethodGet, testURLForHeroes, strings.NewReader("invalid body"))
	responseRecorder := httptest.NewRecorder()
	processGetHeroesRequest(request, responseRecorder)

	if responseRecorder.Code != http.StatusBadRequest {
		t.Error("Request with an invalid body did not produce Bad Request HTTP status code")
	}

	request = httptest.NewRequest(http.MethodGet, testURLForHeroes, nil)
	responseRecorder = httptest.NewRecorder()
	processGetHeroesRequest(request, responseRecorder)

	if responseRecorder.Code != http.StatusOK {
		t.Error("Valid request (with empty body) did not produce OK HTTP status code")
		return
	}

	var retrievedHeroes RetrievedHeroes
	err := json.NewDecoder(responseRecorder.Body).Decode(&retrievedHeroes)
	if err != nil {
		t.Error(err)
		return
	}

	if len(retrievedHeroes.Heroes) != 0 {
		t.Error("Wrong number of heroes returned")
	}

	request = httptest.NewRequest(http.MethodGet, testURLForHeroes, strings.NewReader("[\"first hero\"]"))
	responseRecorder = httptest.NewRecorder()
	processGetHeroesRequest(request, responseRecorder)

	if responseRecorder.Code != http.StatusOK {
		t.Error("Valid request (with one hero in body) did not produce OK HTTP status code")
		return
	}

	err = json.NewDecoder(responseRecorder.Body).Decode(&retrievedHeroes)
	if err != nil {
		t.Error(err)
		return
	}

	if len(retrievedHeroes.Heroes) != 1 {
		t.Error("Wrong number of heroes returned")
		return
	}
	if !reflect.DeepEqual(retrievedHeroes.Heroes[0], firstHero) {
		t.Error("Returned hero does not match")
	}
}
