package herofight

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
)

func makeHero(w http.ResponseWriter, r *http.Request) {
	var newHero Hero
	http.Header.Add(w.Header(), "content-type", "application/json")

	//Decodes body into hero-struct
	err := json.NewDecoder(r.Body).Decode(&newHero)
	if err != nil {
		http.Error(w, "Error decoding body: "+err.Error(), http.StatusBadRequest)
		return
	}

	err = validateNewHero(&newHero, w)
	if err != nil {
		return
	}

	//Encodes hero-struct
	w.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(w).Encode(newHero)
	if err != nil {
		http.Error(w, "Error encoding hero: "+err.Error(), http.StatusInternalServerError)
		return
	}

	//Saves hero in database
	err = DatabaseInstance.SaveHero(&newHero)
	if err != nil {
		http.Error(w, "Error saving hero: "+err.Error(), http.StatusInternalServerError)
	}
}

// Validates the new hero and writes an error message to the response
// if there is a validation error.
func validateNewHero(newHero *Hero, w http.ResponseWriter) error {
	//Gives error if there is no name
	if newHero.Name == "" {
		http.Error(w, "No hero name", http.StatusBadRequest)
		return errors.New("no hero name")
	}

	//Gives error if any of the stats are missing
	if newHero.Stats.Intelligence == "" || newHero.Stats.Strength == "" || newHero.Stats.Speed == "" ||
		newHero.Stats.Durability == "" || newHero.Stats.Power == "" || newHero.Stats.Combat == "" {
		http.Error(w, "Not all stats are filled", http.StatusBadRequest)
		return errors.New("not all stats are filled")
	}

	err = validateHeroStats(&newHero.Stats)
	if err != nil {
		http.Error(w, "All stats must be a string with an integer between 0 and 100", http.StatusBadRequest)
		return errors.New("all stats must be a string with an integer between 0 and 100")
	}
	return nil
}

// Gives error if any stat is not an integer between 0 and 100
func validateHeroStats(stats *Powerstats) error {
	i, iErr := strconv.Atoi(stats.Intelligence)
	st, stErr := strconv.Atoi(stats.Strength)
	sp, spErr := strconv.Atoi(stats.Speed)
	d, dErr := strconv.Atoi(stats.Durability)
	p, pErr := strconv.Atoi(stats.Power)
	c, cErr := strconv.Atoi(stats.Combat)
	if isNotAnyNil(iErr, stErr, spErr, dErr, pErr, cErr) || isAnyNotBetween0And100(i, st, sp, d, p, c) {
		return errors.New("invalid hero stat")
	}
	return nil
}

// Returns true if any of the specified numbers is
// NOT between 0 and 100 (inclusive). Returns false otherwise.
func isAnyNotBetween0And100(numbers ...int) bool {
	for _, number := range numbers {
		if number < 0 || number > 100 {
			return true
		}
	}
	return false
}

// Returns true if any of the specified objects is not nil.
// Returns false otherwise.
func isNotAnyNil(objects ...interface{}) bool {
	for _, object := range objects {
		if object != nil {
			return true
		}
	}
	return false
}
