package herofight

import (
	"encoding/json"
	"io"
	"net/http"
)

// If method is GET, retrieve characters from the Superhero API,
// if method is POST, users can make their own heroes:

// HeroesHandler (to remove golint complaints)
func HeroesHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		processGetHeroesRequest(r, w)

	case http.MethodPost:
		makeHero(w, r)

	default:
		http.Error(w, "Only GET and POST requests are allowed for this endpoint", http.StatusMethodNotAllowed)
	}
}

// Retrieves characters from Superhero API
func processGetHeroesRequest(r *http.Request, w http.ResponseWriter) {
	http.Header.Add(w.Header(), "content-type", "application/json")
	var err error
	var chosenHeroes []string // Array of character names

	err = json.NewDecoder(r.Body).Decode(&chosenHeroes) // Decodes array from request body

	// If no payload, retrieve all characters from database, function is in firebase.go
	if err == io.EOF {
		// Gets all characters from database, function is in firebase.go
		var databaseHeroes, _ = DatabaseInstance.GetAllDBHeroes()

		retrievedHeroes := RetrievedHeroes{Heroes: databaseHeroes}
		err = json.NewEncoder(w).Encode(retrievedHeroes)
		if err != nil {
			http.Error(w, "Error encoding database heroes", http.StatusInternalServerError)
		}

		// If payload, retrieve characters from Superhero API using payload names
	} else {
		if err != nil {
			http.Error(w, "Error decoding array from request body", http.StatusBadRequest)
			return
		}
		heroes := getHeroes(chosenHeroes) // Retrieve characters with payload names

		err = json.NewEncoder(w).Encode(heroes)
		if err != nil {
			http.Error(w, "Error encoding heroes", http.StatusInternalServerError)
		}
	}
}

// TeamHandler (to remove golint complaints)
func TeamHandler(w http.ResponseWriter, r *http.Request) {
	var err error

	// If POST, create team, if GET, retrieve all teams from database
	switch r.Method {
	case http.MethodPost:
		http.Header.Add(w.Header(), "content-type", "application/json")

		var requestTeam RequestTeam // Struct of payload team data
		err = json.NewDecoder(r.Body).Decode(&requestTeam)

		// If payload is correct, make team with payload name and array of character names/IDs
		if err != nil {
			http.Error(w, "Error decoding array from request body, "+err.Error(), http.StatusBadRequest)
			return
		}

		w.WriteHeader(http.StatusCreated)
		err = json.NewEncoder(w).Encode(teamUpHeroes(requestTeam.Name, requestTeam.Heroes))
		if err != nil {
			http.Error(w, "Error encoding teams", http.StatusInternalServerError)
		}

	case http.MethodGet:
		http.Header.Add(w.Header(), "content-type", "application/json")

		// Gets all teams from database, function is in firebase.go
		var databaseTeams, err = DatabaseInstance.GetAllDBTeams()
		if err != nil {
			http.Error(w, "Error retrieving teams from database", http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(w).Encode(databaseTeams)
		if err != nil {
			http.Error(w, "Error encoding database teams", http.StatusInternalServerError)
		}

	default:
		http.Error(w, "Only GET and POST requests are allowed for this endpoint", http.StatusMethodNotAllowed)
	}
}

// FightHandler (to remove golint complaints)
func FightHandler(w http.ResponseWriter, r *http.Request) {
	var err error

	// If POST, create fights between teams given by names in request body,
	// if GET, retrieve all fights in the database
	switch r.Method {
	case http.MethodPost:
		http.Header.Add(w.Header(), "content-type", "application/json")
		var chosenTeams []string

		err = json.NewDecoder(r.Body).Decode(&chosenTeams) // Decodes array from request body

		if err != nil {
			http.Error(w, "Error decoding array from request body, "+err.Error(), http.StatusBadRequest)
			return
		}

		w.WriteHeader(http.StatusCreated)
		err = json.NewEncoder(w).Encode(heroFight(chosenTeams))
		if err != nil {
			http.Error(w, "Error encoding fight", http.StatusInternalServerError)
			return
		}

	case http.MethodGet:
		http.Header.Add(w.Header(), "content-type", "application/json")

		// Gets all characters from database, function is in firebase.go
		var databaseFights, err = DatabaseInstance.GetAllDBFights()
		if err != nil {
			http.Error(w, "Error retrieving fights from database", http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(w).Encode(databaseFights)
		if err != nil {
			http.Error(w, "Error encoding fights", http.StatusInternalServerError)
			return
		}

	default:
		http.Error(w, "Only GET and POST requests are allowed for this endpoint", http.StatusMethodNotAllowed)
	}
}

// StatusHandler (to remove golint complaints)
func StatusHandler(w http.ResponseWriter, r *http.Request) {
	//If GET, check status. Else gives an error
	if r.Method == http.MethodGet {
		checkStatus(w)
	} else {
		http.Error(w, "Only GET requests are allowed for this endpoint", http.StatusMethodNotAllowed)
	}
}
