# gruppe 6 JOLt

[![Build Status](https://img.shields.io/bitbucket/pipelines/Daerikus/gruppe-6-jolt.svg)](https://bitbucket.org/Daerikus/gruppe-6-jolt/addon/pipelines/home)
[![Report card](https://goreportcard.com/badge/bitbucket.org/Daerikus/gruppe-6-jolt)](https://goreportcard.com/report/bitbucket.org/Daerikus/gruppe-6-jolt)

# Super Herio Maker
Using this API, you can create your own heroes or villains, retrieve existing characters from the Superhero API, 
make teams consisting of whichever characters you want, and make the teams fight to see who would win.
The teams can consist of individual characters. 

## Endpoints and instructions:  

### http://10.212.138.62:8080/heromaker/v1/characters:  
#### GET with array of names of existing characters in the Superhero API as request body:
Add those characters to the database. Characters need to be added to the database before they can be teamed up.
Spaces are supported and are required for Superhero API characters with names that includes spaces.
Names are supported in both lowercase and uppercase, but the name has to be accurate.
("spider-man" is the same as "Spider-Man", "spider man" or "Spiderman" are not accepted)
If several characters in the Superhero API have the same name, all of those are added to the database. 
An explanation for how this is handled is in the description for the heroteams endpoint.  
Example request body: ["batman", "Iron Man", "spider-man", ]

#### GET with no request body:
Responds with array of all characters, including all character data, that have been added to the database.
Use this to find Superhero API characters' IDs that are used in the teams endpoint.

#### POST with struct of character information as request body:
Add a custom character with this information to the database. 
The character (hero/villain) name and powerstats numbers (0-100 int as string) are required, all other fields are optional and can be omitted entirely from the body.
ID should not be included, as it could match and interfere with the ID of a Superhero API character.  
Example request body:  
{  
"name": "hero name example",  
"biography": {  
"full-name": "full name example",  
"place-of-birth": "place example",  
"alignment": "good or bad"  
},  
"work": {  
"occupation": "regular job example",  
"base": "hero/villain base example"  
},  
"powerstats": {  
"intelligence": "67",  
"strength": "15",  
"speed": "29",  
"durability": "34",  
"power": "42",  
"combat": "58"  
},  
"appearance": {  
"gender": "gender example",  
"race": "race example (human, alien etc.)",  
"height": ["height in one unit", "height in another unit (optional)"],  
"weight": ["weight in one unit, no other units"],  
"eye-color": "eye color example",  
"hair-color": "hair color example"  
}  
}  

### http://10.212.138.62:8080/heromaker/v1/teams:

#### POST with struct of team information as request body:
Supply a team name and an array of names or IDs of characters that have been saved to the database.
Create a team with this information and save it in the database.
A team can consist of only one character, so 1v1s are possible.
If there are several characters with the same name in the database, use the ID of the characters to differentiate them.
The ID is found using the characters endpoint with GET and no request body.  
Example request body:  
{  
"name": "team name example",  
"characters": ["character1", "character2", "70", "character4"]  
}  

#### GET with no request body:
Responds with an array of all teams, including all team information, from the database.

### http://10.212.138.62:8080/heromaker/v1/fights:

#### POST with array of team names as request body:
One request body array can stage more than one fight. Fights are staged between two and two teams in the array.
If the array contains an odd number of teams, the last team is ignored.
Fights with this information are added to the database.  
Example request body: ["team1fight1", "team2fight1", "team1fight2", "team2fight2"]

#### GET with no request body:
Responds with an array of all fights, including all fight information, from the database.

### OpenAPI specification

If you'd like to see a more structured view of the API, check out the [OpenAPI specification](https://bitbucket.org/Daerikus/gruppe-6-jolt/src/cbd09434ab70/openapi.yaml?at=master).

## Project information

### Completion of goals
We completed all our original goals for the project, which were described in the application description.

### Project ups and downs  

Some group members were unavailable for a time during project work, but the other group members compensated for the missing work and were able to complete the project.
The work compensation was the hardest aspect of the project work, but the group members worked well together.

### What we have learned

We have learned how to use docker, and we are more confident using Firebase and OpenStack.  

### Total work hours
The cumulative project work hours for the group is approximately 73 hours.

## Deployment
Deployment is done using [Terraform](https://terraform.io) which enables Infrastructure as Code for virtually any platform. Most of the deployment process is automated thanks to this tool. 

The service is packaged as a Docker container, you can build the image with `make docker-build`, and run the container with `make docker-run`.

A single server in NTNU's local OpenStack cloud is used to host the API, which can be reached at the following address: <http://http://10.212.138.62:8080/heromaker/v1>.

### Steps

1. [Install Terraform](https://learn.hashicorp.com/terraform/getting-started/install.html)
1. `git clone https://bitbucket.org/Daerikus/gruppe-6-jolt`
1. `cd gruppe-6-jolt/terraform`
1. `terraform init`
1. `terraform apply -var 'openstack_password=YOUR_OPENSTACK_PASSWORD' -var 'key_pair_name=YOUR_KEY_PAIR_NAME' -var 'ssh_ip=YOUR_IP'`
1. Type `yes` and press `Enter`
1. `INSTANCE_IP_ADDRESS=$(terraform output instance_ip_address)`
1. `ssh -i path/to/YOUR_KEY_PAIR_NAME.pem ubuntu@${INSTANCE_IP_ADDRESS}`. If you get `Connection refused`, wait a minute and try running the command again.
1. Make sure the init script has finished running: run `tail -f /var/log/cloud-init-output.log` and wait until a line like `Cloud-init v. 18.2 finished...`  shows up.
1. `cd /super-herio-maker`
1. `sudo nano imt2681-project-firebase-adminsdk-oe91e-92ff1253cb.json`
1. Copy the FireStore access key file's content, press `CTRL+X`, `y` and then `Enter`.
1. `sudo make`
