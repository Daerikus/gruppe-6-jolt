IMAGE_NAME ?= superheroes/superheroes
CONTAINER_NAME ?= superheroes

all:
	make docker-build && make docker-run

docker-build:
	docker build -t $(IMAGE_NAME):latest .

docker-run:
	docker run -d -p 8080:8080 --name $(CONTAINER_NAME) $(IMAGE_NAME):latest
