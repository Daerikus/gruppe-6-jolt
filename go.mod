module bitbucket.org/Daerikus/gruppe-6-jolt

go 1.13

require (
	cloud.google.com/go/firestore v1.0.0
	cloud.google.com/go/storage v1.2.1 // indirect
	firebase.google.com/go v3.10.0+incompatible
	github.com/pkg/errors v0.8.1
	google.golang.org/api v0.13.0
)
