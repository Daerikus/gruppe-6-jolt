terraform {
  required_version = ">= 0.12.8"
}

provider "openstack" {
  user_name         = "bendegua"
  password          = var.openstack_password
  tenant_id         = "1a8fefc0c8ef49e8bc365c318ba3f021"
  tenant_name       = "IMT2681_H19_bendegua"
  user_domain_name  = "NTNU"
  project_domain_id = "cb782810849b4ce8bce7f078cc193b19"
  auth_url          = "https://api.skyhigh.iik.ntnu.no:5000/v3"
  region            = "SkyHiGh"
}

resource "openstack_compute_instance_v2" "super_herio_maker_instance" {
  flavor_name = "m1.tiny"
  image_name  = "Ubuntu Server 18.04 LTS (Bionic Beaver) amd64"
  name        = "super_herio_maker"
  key_pair    = var.key_pair_name
  user_data   = file("${path.module}/prepare_service.sh")

  network {
    port = openstack_networking_port_v2.super_herio_maker_instance_port.id
  }
}