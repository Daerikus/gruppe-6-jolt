resource "openstack_networking_secgroup_v2" "http_security_group" {
  name        = "http_security_group"
  description = "Allows incoming HTTP traffic on ports 80 and 8080, using both IPv4 and IPv6."
}

resource "openstack_networking_secgroup_rule_v2" "http_security_group_rule_1" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.http_security_group.id
}

resource "openstack_networking_secgroup_rule_v2" "http_security_group_rule_2" {
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "::/0"
  security_group_id = openstack_networking_secgroup_v2.http_security_group.id
}

resource "openstack_networking_secgroup_rule_v2" "http_security_group_rule_3" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8080
  port_range_max    = 8080
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.http_security_group.id
}

resource "openstack_networking_secgroup_rule_v2" "http_security_group_rule_4" {
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = 8080
  port_range_max    = 8080
  remote_ip_prefix  = "::/0"
  security_group_id = openstack_networking_secgroup_v2.http_security_group.id
}

resource "openstack_networking_secgroup_v2" "ssh_security_group" {
  name        = "ssh_security_group"
  description = "Allows incoming SSH traffic (port 22) from the specified IP address."
}

resource "openstack_networking_secgroup_rule_v2" "ssh_security_group_rule_1" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = var.ssh_ip
  security_group_id = openstack_networking_secgroup_v2.ssh_security_group.id
}
