output "instance_ip_address" {
  description = "Floating IPv4 address associated with the created instance"
  value       = openstack_networking_floatingip_v2.floating_ip_for_super_herio_maker_instance.address
}
