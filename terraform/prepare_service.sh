#!/usr/bin/env sh

export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get -y upgrade
apt-get remove docker docker-engine docker.io
apt-get install git docker.io make -y

systemctl start docker
systemctl enable docker

git clone https://bitbucket.org/Daerikus/gruppe-6-jolt "/super-herio-maker"

# TODO figure out how to distribute credentials securily so the script can start the service
