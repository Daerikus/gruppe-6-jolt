variable "openstack_password" {
  type = string
}

variable "key_pair_name" {
  type        = string
  description = "Name of the key pair to use for SSH access."
  default     = "MY-TOP-SECRET-KEY"
}

variable "ssh_ip" {
  type        = string
  description = "The IP address from which SSH access will be allowed."
  default     = "129.241.14.89/28"
}