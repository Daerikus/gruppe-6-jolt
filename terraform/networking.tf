data "openstack_networking_network_v2" "ntnu_internal_network" {
  name = "ntnu-internal"
}

data "openstack_networking_secgroup_v2" "default_security_group" {
  name = "default"
}

resource "openstack_networking_network_v2" "network" {
  name = "super_herio_maker_network"
}

resource "openstack_networking_subnet_v2" "subnet" {
  name       = "subnet_1"
  network_id = openstack_networking_network_v2.network.id
  cidr       = "192.168.180.0/24"
  ip_version = 4
  gateway_ip = "192.168.180.1"
}

resource "openstack_networking_router_v2" "router" {
  name                = "super_herio_maker_network_router"
  external_network_id = data.openstack_networking_network_v2.ntnu_internal_network.id
}

resource "openstack_networking_router_interface_v2" "router_interface" {
  router_id = openstack_networking_router_v2.router.id
  subnet_id = openstack_networking_subnet_v2.subnet.id
}

resource "openstack_networking_floatingip_v2" "floating_ip_for_super_herio_maker_instance" {
  pool        = "ntnu-internal"
  description = "Floating IP associated with the super_herio_maker instance."
  port_id     = openstack_networking_port_v2.super_herio_maker_instance_port.id
}

resource "openstack_networking_port_v2" "super_herio_maker_instance_port" {
  name            = "super_herio_maker_instance_port"
  network_id      = openstack_networking_network_v2.network.id
  security_group_ids= [
    data.openstack_networking_secgroup_v2.default_security_group.id,
    openstack_networking_secgroup_v2.http_security_group.id,
    openstack_networking_secgroup_v2.ssh_security_group.id
  ]

  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.subnet.id
  }
}