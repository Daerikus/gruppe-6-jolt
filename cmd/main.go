package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/Daerikus/gruppe-6-jolt/herofight"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	herofight.DatabaseInstance.InitDB()

	http.HandleFunc("/heromaker/v1/characters", herofight.HeroesHandler)
	http.HandleFunc("/heromaker/v1/teams", herofight.TeamHandler)
	http.HandleFunc("/heromaker/v1/fights", herofight.FightHandler)
	http.HandleFunc("/heromaker/v1/status", herofight.StatusHandler)

	fmt.Println("Listening on port " + port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
